package model;

public class ProductPriceOffsetMessage extends Message {
    private float priceOffSet;

    public float getPriceOffSet() {
        return priceOffSet;
    }

    public void setPriceOffSet(float priceOffSet) {
        this.priceOffSet = priceOffSet;
    }
}
