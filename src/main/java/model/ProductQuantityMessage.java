package model;

import com.sun.istack.internal.NotNull;

public class ProductQuantityMessage extends Message {
    @NotNull
    private int Quantity;

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }
}
