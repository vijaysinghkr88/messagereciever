package model;

import com.sun.istack.internal.NotNull;
import enums.MessageType;

public class Message {
    @NotNull
    private MessageType messageType;
    @NotNull
    private Sale sale;

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public Sale getSale() {
        return sale;
    }

    public void setSale(Sale sale) {
        this.sale = sale;
    }
}
