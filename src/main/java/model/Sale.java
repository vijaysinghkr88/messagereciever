package model;

import com.sun.istack.internal.NotNull;
import enums.ProductType;

public class Sale {
    @NotNull
    private ProductType productType;
    @NotNull
    private float price;

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

}
