package enums;

/*
The list of product set
 */
public enum ProductType {
    PRODUCT_TYPE_1(10.00f),
    PRODUCT_TYPE_2(20.00f),
    PRODUCT_TYPE_3(30.00f),
    PRODUCT_TYPE_4(40.00f);

    private float price;
    private ProductType(float price) {
       this.price = price;
    }

    public float toValue() {
        return this.price;
    }
}
