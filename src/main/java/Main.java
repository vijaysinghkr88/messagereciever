import enums.MessageType;
import enums.ProductType;
import model.Message;
import model.ProductPriceOffsetMessage;
import model.ProductQuantityMessage;
import service.MessageService;
import service.MessageServiceImpl;
import model.Sale;

public class Main {
    public static void main(String[] args) {
        MessageService messageService = new MessageServiceImpl();
        Message message = null;
        for(int i = 1; i <= 100; i++) {
            Sale sale = new Sale();
            if(i%3 == 0) {
                sale.setProductType(ProductType.PRODUCT_TYPE_2);
                sale.setPrice(ProductType.PRODUCT_TYPE_2.toValue());
            } else if (i%4 == 0) {
                sale.setProductType(ProductType.PRODUCT_TYPE_3);
                sale.setPrice(ProductType.PRODUCT_TYPE_3.toValue());
            } else {
                sale.setProductType(ProductType.PRODUCT_TYPE_1);
                sale.setPrice(ProductType.PRODUCT_TYPE_1.toValue());
            }
            if( i == 5 || i == 15  || i == 20) {
                ProductQuantityMessage productQuantityMessage =  new ProductQuantityMessage();
                productQuantityMessage.setSale(sale);
                productQuantityMessage.setMessageType(MessageType.MESSAGE_TYPE_2);
                productQuantityMessage.setQuantity(5);
                message = productQuantityMessage;
            } else if (i == 45) {
                ProductPriceOffsetMessage productPriceOffsetMessage = new ProductPriceOffsetMessage();
                productPriceOffsetMessage.setSale(sale);
                productPriceOffsetMessage.setMessageType(MessageType.MESSAGE_TYPE_3);
                productPriceOffsetMessage.setPriceOffSet(5.00f);
                message = productPriceOffsetMessage;
            } else {
                message = new Message();
                message.setSale(sale);
                message.setMessageType(MessageType.MESSAGE_TYPE_1);
            }
            if (!messageService.recieveMessage(message)) {
                System.out.println("Message Bucket is already full with 50 message");
                break;
            }
        }
    }
}
