package service;

import model.Message;

import java.util.logging.Logger;

public class MessageServiceImpl implements MessageService {
    private static final Logger LOGGER = Logger.getLogger("Logs");
    public boolean recieveMessage(Message message) {
        MessageRegistration messageRegistration = MessageRegistration.getMessageRegistration();
        if (messageRegistration.getMessageBucketSize()  < 50) {
            messageRegistration.updateMessageList(message);
            return true;
        } else {
            LOGGER.info("System has already handle 50 message");
            messageRegistration.printMessages();
            return false;
        }
    }
}
