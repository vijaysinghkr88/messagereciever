package service;

import model.Message;

import java.util.ArrayList;
import java.util.List;

public final class MessageRegistration {
    private static final MessageRegistration messageRegistration
            = new MessageRegistration();
    private static final List<Message> messageList = new ArrayList<Message>();

    private MessageRegistration() {}

    public static MessageRegistration getMessageRegistration() {
        return messageRegistration;
    }

    public static void updateMessageList(Message message) {
        SalesRegistration.getSalesRegistration().registerSale(message);
        messageList.add(message);
        if (messageList.size()%10 == 0) {
            System.out.println("Message bucket size = " + getMessageBucketSize());
            SalesRegistration.printRegistration();
        }
    }

    public static int getMessageBucketSize() {
        return messageList.size();
    }

    public static void printMessages() {
        for(Message message: messageList) {
            System.out.println("Message type = " + message.getMessageType()
                              + " product type = " + message.getSale().getProductType()
            );
        }
    }
}
