package service;

import enums.ProductType;
import model.Message;
import model.ProductPriceOffsetMessage;
import model.ProductQuantityMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class SalesRegistration {

    private static final SalesRegistration salesRegistration = new SalesRegistration();
    private static final Map<ProductType, List<Float>> register = new HashMap();
    private SalesRegistration(){};

    public static SalesRegistration getSalesRegistration() {
        return salesRegistration;
    }

    public static void registerSale(Message message) {
        switch(message.getMessageType()) {
            case MESSAGE_TYPE_1:
                if (register.containsKey(message.getSale().getProductType())) {
                    List<Float> priceList = register.get(message.getSale().getProductType());
                    priceList.add(message.getSale().getPrice());
                    register.put(message.getSale().getProductType(),
                            priceList);
                } else {
                    List<Float> priceList = new ArrayList<Float>();
                    priceList.add(message.getSale().getPrice());
                    register.put(message.getSale().getProductType(),
                            priceList);
                }
                break;
            case MESSAGE_TYPE_2:
                if (register.containsKey(message.getSale().getProductType())) {
                    List<Float> priceList = register.get(message.getSale().getProductType());
                    for(int i = 1; i <= ((ProductQuantityMessage)message).getQuantity(); i ++) {
                        priceList.add(message.getSale().getPrice());
                    }
                    register.put(message.getSale().getProductType(),
                            priceList);
                } else {
                    List<Float> priceList = new ArrayList<Float>();
                    for(int i = 1; i <= ((ProductQuantityMessage)message).getQuantity(); i ++) {
                        priceList.add(message.getSale().getPrice());
                    }
                    register.put(message.getSale().getProductType(),
                            priceList);
                }
                break;
            case MESSAGE_TYPE_3:
                if (register.containsKey(message.getSale().getProductType())) {
                    List<Float> priceList = register.get(message.getSale().getProductType());
                    List<Float> newPriceList = new ArrayList<Float>();
                    for(float nprice: priceList) {
                        nprice = nprice + ((ProductPriceOffsetMessage)message).getPriceOffSet();
                        newPriceList.add(nprice);
                    }
                    register.put(message.getSale().getProductType(),
                            newPriceList);
                }
                break;
                default:
                    break;
        }

    }

    public static void printRegistration() {
        for(ProductType productType: register.keySet()) {
            System.out.println(
                    "For product type = "
                    + productType
                    + " " + " total price sold is "
                    + calculatePrice(register.get(productType))
                    + " "
                    + "total quantity sold is " + register.get(productType).size()
            );
        }
    }

    private static float calculatePrice(List<Float> prcies) {
        float sum = 0.00f;
        for(float price: prcies) {
            sum = sum + price;
        }
        return sum;
    }

}
